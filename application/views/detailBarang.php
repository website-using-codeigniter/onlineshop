<div class="container-fluid">

    <div class="card">
        <h5 class="card-header">Detail Produk</h5>
        <div class="card-body">
            
            <?php foreach($barang as $brg) : ?>
            <div class="row">
                <div class="col-md-4">
                        <img src="<?php echo base_url().'/uploads/'.$brg->Gambar ?> " class="card-img-top" >
                </div>
                <div class="col-md-8">
                    <table class="table">
                        <tr>
                            <td>Nama Produk</td>
                            <td><strong><?php echo $brg->NamaBarang ?></strong></td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td><strong><?php echo $brg->Keterangan ?></strong></td>
                        </tr>
                        <tr>
                            <td>Kategori</td>
                            <td><strong><?php echo $brg->Kategori ?></strong></td>
                        </tr>
                        <tr>
                            <td>Stock</td>
                            <td><strong><?php echo $brg->Stock ?></strong></td>
                        </tr>
                        <tr>
                            <td>Harga</td>
                            <td><strong><div class="btn btn-sm btn-success">Rp. <?php echo number_format($brg->Harga,0,',','.') ?></div></strong></td>
                        </tr>
                    </table>
                    
                    <?php echo anchor ('dashboard/index/','<div class =" btn btn-sm btn-danger">Kembali</div>') ?>
                    <?php echo anchor ('dashboard/tambahKeKeranjang/' .$brg->IdBarang, '<div class =" btn btn-sm btn-primary">Tambah ke Keranjang</div>') ?>
                    
                </div>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>