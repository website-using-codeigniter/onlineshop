<div class="container-fluid">

    <button class="btn btn-sm btn-primary mb-3" data-toggle="modal" data-target="#tambahbarang"><i class="fas fa-plus fa-sm" ></i>Tambah Barang</button>
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nama Barang</th>
            <th>Keterangan</th>
            <th>Kategori</th>
            <th>Harga</th>
            <th>Stock</th>
            <th colspan="3">Aksi</th>
        </tr>
        <?php
        $no=1;
        foreach ($barang as $brg) : ?>
        <tr>
        <td><?php echo $no++ ?></td>
        <td><?php echo $brg->NamaBarang ?></td>
        <td><?php echo $brg->Keterangan ?></td>
        <td><?php echo $brg->Kategori ?></td>
        <td><?php echo $brg->Harga ?></td>
        <td><?php echo $brg->Stock ?></td>
        <td><div class="btn btn-success btn-sm"><i class="fas fa-search-plus"></i></div></td>
        <td><?php echo anchor('admin/dataBarang/edit/' .$brg->IdBarang, '<div class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></div>') ?></td>
        <td><?php echo anchor('admin/dataBarang/hapus/' .$brg->IdBarang, '<div class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></div>') ?></td>
        </tr>
            <?php endforeach ;?>
    </table>

</div>

<!-- Modal -->
<div class="modal fade" id="tambahbarang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Input Produk</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="<?php echo base_url ().'admin/dataBarang/tambahAksi'?>" method="post" enctype="multipart/form-data">
            
            <div class="form-grup">
                <label >Nama Barang</label>
                <input type="text" name="NamaBarang" class="form-control">
            </div>
            <div class="form-grup">
                <label >Keterangan</label>
                <input type="text" name="Keterangan" class="form-control">
            </div>
            <div class="form-grup">
                <label >Kategori</label>
                <select class="form-control" name="kategori">
                    <option>Elektronik</option>
                    <option>Pakaian</option>
                </select>
            </div>
            <div class="form-grup">
                <label >Harga</label>
                <input type="text" name="Harga" class="form-control">
            </div>
            <div class="form-grup">
                <label >Stock</label>
                <input type="text" name="Stock" class="form-control">
            </div>
            <div class="form-grup">
                <label >Gambar Produk</label><br>
                <input type="file" name="Gambar" class="form-control">
            </div>

            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>