<div class="container-fluid">
    <h3><i class="fas fa-edit"></i>Edit Data Barang</h3>

    <?php foreach ($barang as $brg ) : ?>

        <form method="post" action="<?php echo base_url().'admin/dataBarang/update'?>">
        
        <div class="for-group">
            <label>Nama Barang</label>
            <input type="text" name="NamaBarang" class="form-control" value="<?php echo $brg->NamaBarang ?>">
        </div>
        <div class="for-group">
            <label>Keterangan</label>
            <input type="hidden" name="IdBarang" class="form-control" value="<?php echo $brg->IdBarang ?>">
            <input type="text" name="Keterangan" class="form-control" value="<?php echo $brg->Keterangan ?>">
        </div>
        <div class="for-group">
            <label>Kategori</label>
            <input type="text" name="Kategori" class="form-control" value="<?php echo $brg->Kategori ?>">
        </div>
        <div class="for-group">
            <label>Harga</label>
            <input type="text" name="Harga" class="form-control" value="<?php echo $brg->Harga ?>">
        </div>
        <div class="for-group">
            <label>Stock</label>
            <input type="text" name="Stock" class="form-control" value="<?php echo $brg->Stock ?>">
        </div>

        <button type="submit" class="btn btn-primary btn-sm mt-3">Simpan</button>

        </form>
        
        <?php endforeach?>
</div>