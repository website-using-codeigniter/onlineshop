<?php

class ModelKategori extends CI_Model{

    public function dataElektronik(){
        return $this->db->get_where("tb_barang",array('kategori' => 'elektronik'));
    }

    public function dataPakaian(){
        return $this->db->get_where("tb_barang",array('kategori' => 'pakaian'));
    }
}