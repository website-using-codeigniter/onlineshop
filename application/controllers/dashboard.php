<?php

class Dashboard extends CI_Controller{

    public function __construct(){
        parent::__construct();

        if($this->session->userdata('roleId') != '2'){
            $this->session->set_flashdata('pesan','<div class=" text-center alert alert-danger alert-dismissible fade show" role="alert">
            Belum Login<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button></div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        $data['barang'] = $this->modelBarang->tampilData()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('dashboard',$data);
        $this->load->view('templates/footer');
    }

    public function tambahKeKeranjang($id){

        $barang = $this->modelBarang->find($id);
        $data = array(
            'id'    => $barang->IdBarang,
            'qty'   => 1,
            'price' => $barang->Harga,
            'name'  => $barang->NamaBarang

        );

        $this->cart->insert($data);
        redirect('dashboard');
    }

    public function detail($IdBarang){

        $data['barang'] = $this->modelBarang->detailBarang($IdBarang);
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('detailBarang',$data);
        $this->load->view('templates/footer');
    }

    public function detailKeranjang(){

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('keranjang');
        $this->load->view('templates/footer');
    }

    public function hapusKeranjang(){

        $this->cart->destroy();
        redirect('dashboard/index');
    }

    public function pembayaran(){

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pembayaran');
        $this->load->view('templates/footer');

    }

    public function prosesPesanan(){

        $is_processed = $this->modelInvoice->index();
        if ($is_processed) {
            $this->cart->destroy();
            $this->load->view('templates/header');
            $this->load->view('templates/sidebar');
            $this->load->view('prosesPesanan');
            $this->load->view('templates/footer');
        }else {
            echo "Pesanan Anda Gagal";
        }
        }


        
}