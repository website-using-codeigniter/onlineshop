<?php

class Registrasi extends CI_Controller{

    public function index()
    {

        $this->form_validation->set_rules('nama','nama','required');
        $this->form_validation->set_rules('username','username','required');
        $this->form_validation->set_rules('password1','Password','required|matches[password2]');
        $this->form_validation->set_rules('password2','Password','required|matches[password1]');

        if($this->form_validation->run() == FALSE){
            $this->load->view('templates/header');
            $this->load->view('registrasi');
            $this->load->view('templates/footer');
        }else{
            $data = array(
                'id'        => '',
                'nama'      => $this->input->post('nama'),
                'username'  => $this->input->post('username'),
                'password'  => $this->input->post('password1'),
                'roleId'    => 2,
            );

            $this->db->insert('tb_user',$data);
            redirect('auth/login');
        }
        
    }
}