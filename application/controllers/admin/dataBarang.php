<?php 

class DataBarang extends CI_Controller{

    public function __construct(){
        parent::__construct();

        if($this->session->userdata('roleId') != '1'){
            $this->session->set_flashdata('pesan','<div class=" text-center alert alert-danger alert-dismissible fade show" role="alert">
            Belum Login<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button></div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        $data['barang'] = $this->modelBarang->tampilData()->result();
        $this->load->view('templatesAdmin/header');
        $this->load->view('templatesAdmin/sidebar');
        $this->load->view('admin/dataBarang',$data);
        $this->load->view('templatesAdmin/footer');
    }

    public function tambahAksi(){

        $NamaBarang = $this->input->post('NamaBarang');
        $Keterangan = $this->input->post('Keterangan');
        $Kategori   = $this->input->post('Kategori');
        $Harga      = $this->input->post('Harga');
        $Stock      = $this->input->post('Stock');
        $Gambar     = $_FILES['Gambar']['name'];
        if($Gambar=''){}else{
            $config ['upload_path'] = './uploads';
            $config ['allowed_types'] = 'jpg|jpeg|png|gif';

            $this->load->library('upload',$config);
            if(!$this->upload->do_upload('Gambar')){
                echo "Upload Gagal!";
            }else{
                $Gambar=$this->upload->data('file_name');
            }

        }

        //memasukkan data kedalam array

        $data = array(
            'NamaBarang' =>$NamaBarang,
            'Keterangan' =>$Keterangan,
            'Kategori' =>$Kategori,
            'Harga' =>$Harga,
            'Stock' =>$Stock,
            'Gambar' =>$Gambar
        );

        $this->modelBarang->tambahBarang($data,'tb_barang');
        redirect('admin/dataBarang/index');

    }

    public function edit($id){
        $where =array ('IdBarang' =>$id);
        $data['barang'] = $this->modelBarang->editBarang($where, 'tb_barang')->result();
        $this->load->view('templatesAdmin/header');
        $this->load->view('templatesAdmin/sidebar');
        $this->load->view('admin/editBarang',$data);
        $this->load->view('templatesAdmin/footer');
    }

    public function update(){
        $id             = $this->input->post('IdBarang');
        $NamaBarang     = $this->input->post('NamaBarang');
        $Keterangan     = $this->input->post('Keterangan');
        $Kategori       = $this->input->post('Kategori');
        $Harga          = $this->input->post('Harga');
        $Stock          = $this->input->post('Stock');

        $data = array (

            'NamaBarang' =>$NamaBarang,
            'Keterangan' =>$Keterangan,
            'Kategori' =>$Kategori,
            'harga' =>$Harga,
            'Stock' =>$Stock
        );

        $where = array(
            'IdBarang' => $id
        );

        $this->modelBarang->updateData($where,$data,'tb_barang');
        redirect('admin/dataBarang/index');
    }

    public function hapus($id){

        $where = array('IdBarang' => $id);
        $this->modelBarang->hapusData($where,'tb_barang');
        redirect('admin/dataBarang/index');
    }
}