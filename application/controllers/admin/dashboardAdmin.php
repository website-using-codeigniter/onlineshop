<?php

class DashboardAdmin extends CI_Controller{
    
    public function __construct(){
        parent::__construct();

        if($this->session->userdata('roleId') != '1'){
            $this->session->set_flashdata('pesan','<div class=" text-center alert alert-danger alert-dismissible fade show" role="alert">
            Belum Login<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button></div>');
            redirect('auth/login');
        }
    }

    public function index ()
    {
        $this->load->view('templatesAdmin/header');
        $this->load->view('templatesAdmin/sidebar');
        $this->load->view('admin/dashboard');
        $this->load->view('templatesAdmin/footer');
    }
}