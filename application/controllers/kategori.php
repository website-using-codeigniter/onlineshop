<?php

class Kategori extends CI_Controller{

    public function elektronik()
    {
        $data['elektronik'] = $this->modelKategori->dataElektronik()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('elektronik',$data);
        $this->load->view('templates/footer');

    }

    public function pakaian()
    {
        $data['pakaian'] = $this->modelKategori->dataPakaian()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pakaian',$data);
        $this->load->view('templates/footer');

    }
}